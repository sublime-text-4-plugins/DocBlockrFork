# -*- coding: utf-8 -*-
"""DocBlockrFork plugin.
"""
from .plugins import events
from .plugins import settings
from python_utils.sublime_text_utils import settings as settings_utils

from .plugins import *  # noqa


class DocBlockrForkProjectSettingsController(settings_utils.ProjectSettingsController):
    """Project settings controller.
    """
    @settings_utils.distinct_until_buffer_changed
    def on_post_save_async(self, view):
        """Called after a view has been saved.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        self._on_post_save_async(view, settings)


def plugin_loaded():
    """On plugin loaded callback.
    """
    events.broadcast("plugin_loaded")


def plugin_unloaded():
    """On plugin unloaded.
    """
    events.broadcast("plugin_unloaded")


if __name__ == "__main__":
    pass
