# -*- coding: utf-8 -*-
import datetime
import os
import re
import time

import sublime
import sublime_plugin

from python_utils import logging_system
from python_utils.lazy_import import lazy_module
from python_utils.sublime_text_utils import settings as settings_utils
from python_utils.sublime_text_utils.events import Events
from python_utils.sublime_text_utils.queue import Queue

# NOTE: The SPHINX_BUILDING_DOCS environment variable is meant to be set inside the
# conf.py file of a Sphinx project.
sphinx_building_docs = "SPHINX_BUILDING_DOCS" in os.environ

if sphinx_building_docs:
    from .parsers import actionscript
    from .parsers import coffeescript
    from .parsers import cpp
    from .parsers import java
    from .parsers import javascript
    from .parsers import objective_c
    from .parsers import php
    from .parsers import rust
    from .parsers import typescript
else:
    actionscript = lazy_module("DocBlockrFork.plugins.parsers.actionscript")
    coffeescript = lazy_module("DocBlockrFork.plugins.parsers.coffeescript")
    cpp = lazy_module("DocBlockrFork.plugins.parsers.cpp")
    java = lazy_module("DocBlockrFork.plugins.parsers.java")
    javascript = lazy_module("DocBlockrFork.plugins.parsers.javascript")
    objective_c = lazy_module("DocBlockrFork.plugins.parsers.objective_c")
    php = lazy_module("DocBlockrFork.plugins.parsers.php")
    rust = lazy_module("DocBlockrFork.plugins.parsers.rust")
    typescript = lazy_module("DocBlockrFork.plugins.parsers.typescript")


__all__ = [
    "DocBlockrForkCommand",
    "DocBlockrForkIndentCommand",
    "DocBlockrForkJoinCommand",
    "DocBlockrForkDecorateCommand",
    "DocBlockrForkDeindentCommand",
    "DocBlockrForkReparseCommand",
    "DocBlockrForkTrimAutoWhitespaceCommand",
    "DocBlockrForkWrapLinesCommand",
]

root_folder = os.path.realpath(
    os.path.abspath(
        os.path.join(
            os.path.normpath(os.path.join(os.path.dirname(__file__), os.pardir))
        )
    )
)

queue = Queue()
events = Events()

package_name = os.path.basename(root_folder)
plugin_name = "DocBlockrFork"
logger: logging_system.Logger = logging_system.Logger(
    logger_name=plugin_name,
    use_file_handler=os.path.join(root_folder, "tmp", "logs"),
)
settings = settings_utils.SettingsManager(events=events, logger=logger)


def set_logging_level():
    """Summary"""
    try:
        logger.set_logging_level(
            logging_level=settings.get("doc_blockr_fork.logging_level", "INFO")
        )
    except Exception as err:
        print(__file__, err)


@events.on("plugin_loaded")
def on_plugin_loaded():
    """Summary"""
    queue.debounce(
        settings.load,
        delay=100,
        key=f"{plugin_name}-debounce-settings-load",
    )
    queue.debounce(
        set_logging_level,
        delay=200,
        key=f"{plugin_name}-debounce-set-logging-level",
    )


@events.on("plugin_unloaded")
def on_plugin_unloaded():
    """Summary"""
    settings.unobserve()
    queue.unload()
    events.destroy()


def write(view, str):
    view.run_command("insert_snippet", {"contents": str})


def counter():
    count = 0
    while True:
        count += 1
        yield (count)


def escape(str):
    return str.replace("$", "\\$").replace("{", "\\{").replace("}", "\\}")


def get_parser(view):
    scope = view.scope_name(view.sel()[0].end())
    res = re.search("\\bsource\\.([a-z+\\-]+)", scope)
    source_lang = res.group(1) if res else "js"

    if source_lang == "php":
        return php.DocBlockrForkPHP(settings)
    elif source_lang == "coffee":
        return coffeescript.DocBlockrForkCoffeescript(settings)
    elif source_lang in ("actionscript", "haxe"):
        return actionscript.DocBlockrForkActionscript(settings)
    elif source_lang in ("c++", "c", "cuda-c++"):
        return cpp.DocBlockrForkCPP(settings)
    elif source_lang in ("objc", "objc++"):
        return objective_c.DocBlockrForkObjectiveC(settings)
    elif source_lang in ("java", "groovy", "apex"):
        return java.DocBlockrForkJava(settings)
    elif source_lang == "rust":
        return rust.DocBlockrForkRust(settings)
    elif source_lang == "ts":
        return typescript.DocBlockrForkTypescript(settings)

    return javascript.DocBlockrForkJavascript(settings)


def get_doc_block_region(view, point):
    """
    Given a starting point inside a DocBlock, return a Region which encompasses the entire block.
    This is similar to `run_command('expand_selection', { to: 'scope' })`, however it is resilient to bugs which occur
    due to language files adding scopes inside the DocBlock (eg: to highlight tags)
    """
    start = end = point
    while start > 0 and view.scope_name(start - 1).find("comment.block") > -1:
        start = start - 1

    while end < view.size() and view.scope_name(end).find("comment.block") > -1:
        end = end + 1

    return sublime.Region(start, end)


class DocBlockrForkCommand(sublime_plugin.TextCommand):
    def run(self, edit, inline=False):

        self.initialize(self.view, inline)

        if self.parser.isExistingComment(self.line):
            write(self.view, "\n *" + self.indentSpaces)
            return

        # erase characters in the view (will be added to the output later)
        self.view.erase(edit, self.trailingRgn)

        # match against a function declaration.
        out = self.parser.parse(self.line)

        snippet = self.generateSnippet(out, inline)

        write(self.view, snippet)

    def initialize(self, v, inline=False):
        point = v.sel()[0].end()

        # trailing characters are put inside the body of the comment
        self.trailingRgn = sublime.Region(point, v.line(point).end())
        self.trailingString = v.substr(self.trailingRgn).strip()
        # drop trailing '*/'
        self.trailingString = escape(re.sub("\\s*\\*\\/\\s*$", "", self.trailingString))

        self.indentSpaces = " " * max(
            0, settings.get("doc_blockr_fork.indentation_spaces", 1)
        )
        self.prefix = "*"

        settingsAlignTags = settings.get("doc_blockr_fork.align_tags", "deep")
        self.deepAlignTags = settingsAlignTags == "deep"
        self.shallowAlignTags = settingsAlignTags in ("shallow", True)

        self.parser = parser = get_parser(v)
        parser.inline = inline

        # use trailing string as a description of the function
        if self.trailingString:
            parser.setNameOverride(self.trailingString)

        # read the next line
        self.line = parser.getDefinition(v, v.line(point).end() + 1)

    def generateSnippet(self, out, inline=False):
        # substitute any variables in the tags

        if out:
            out = self.substituteVariables(out)

        # align the tags
        if out and (self.shallowAlignTags or self.deepAlignTags) and not inline:
            out = self.alignTags(out)

        # fix all the tab stops so they're consecutive
        if out:
            out = self.fixTabStops(out)

        if inline:
            if out:
                return " " + out[0] + " */"
            else:
                return " $0 */"
        else:
            return self.createSnippet(out) + (
                "\n" if settings.get("doc_blockr_fork.newline_after_block") else ""
            )

    def alignTags(self, out):
        def outputWidth(str):
            # get the length of a string, after it is output as a snippet,
            # "${1:foo}" --> 3
            return len(re.sub("[$][{]\\d+:([^}]+)[}]", "\\1", str).replace("\\$", "$"))

        # count how many columns we have
        maxCols = 0
        # this is a 2d list of the widths per column per line
        widths = []

        # Grab the return tag if required.
        if settings.get("doc_blockr_fork.per_section_indent"):
            returnTag = settings.get("doc_blockr_fork.return_tag") or "@return"
        else:
            returnTag = False

        for line in out:
            if line.startswith("@"):
                # Ignore the return tag if we're doing per-section indenting.
                if returnTag and line.startswith(returnTag):
                    continue
                # ignore all the words after `@author`
                columns = (
                    line.split(" ") if not line.startswith("@author") else ["@author"]
                )
                widths.append(list(map(outputWidth, columns)))
                maxCols = max(maxCols, len(widths[-1]))

        #  initialise a list to 0
        maxWidths = [0] * maxCols

        if self.shallowAlignTags:
            maxCols = 1

        for i in range(0, maxCols):
            for width in widths:
                if i < len(width):
                    maxWidths[i] = max(maxWidths[i], width[i])

        # Convert to a dict so we can use .get()
        maxWidths = dict(enumerate(maxWidths))

        # Minimum spaces between line columns
        minColSpaces = settings.get("doc_blockr_fork.min_spaces_between_columns", 1)

        for index, line in enumerate(out):
            # format the spacing of columns, but ignore the author tag. (See #197)
            if line.startswith("@") and not line.startswith("@author"):
                newOut = []
                for partIndex, part in enumerate(line.split(" ")):
                    newOut.append(part)
                    newOut.append(
                        " " * minColSpaces
                        + (" " * (maxWidths.get(partIndex, 0) - outputWidth(part)))
                    )
                out[index] = "".join(newOut).strip()

        return out

    def substituteVariables(self, out):
        def getVar(match):
            varName = match.group(1)
            if varName == "datetime":
                date = datetime.datetime.now().replace(microsecond=0)
                offset = time.timezone / -3600.0
                return "%s%s%02d%02d" % (
                    date.isoformat(),
                    "+" if offset >= 0 else "-",
                    abs(offset),
                    (offset % 1) * 60,
                )
            elif varName == "date":
                return datetime.date.today().isoformat()
            else:
                return match.group(0)

        def subLine(line):
            return re.sub(r"\{\{([^}]+)\}\}", getVar, line)

        return list(map(subLine, out))

    def fixTabStops(self, out):
        tabIndex = counter()

        def swapTabs(m):
            return "%s%d%s" % (m.group(1), next(tabIndex), m.group(2))

        for index, outputLine in enumerate(out):
            out[index] = re.sub("(\\$\\{)\\d+(:[^}]+\\})", swapTabs, outputLine)

        return out

    def createSnippet(self, out):
        snippet = ""
        closer = self.parser.options["commentCloser"]
        if out:
            if settings.get("doc_blockr_fork.spacer_between_sections"):
                lastTag = None
                for idx, line in enumerate(out):
                    res = re.match("^\\s*@([a-zA-Z]+)", line)
                    if res and (lastTag != res.group(1)):
                        if not settings.get("doc_blockr_fork.function_description"):
                            if lastTag is not None:
                                out.insert(idx, "")
                        else:
                            out.insert(idx, "")
                        lastTag = res.group(1)
            elif settings.get(
                "doc_blockr_fork.spacer_between_sections"
            ) == "after_description" and settings.get(
                "doc_blockr_fork.function_description"
            ):
                lastLineIsTag = False
                for idx, line in enumerate(out):
                    res = re.match("^\\s*@([a-zA-Z]+)", line)
                    if res:
                        if not lastLineIsTag:
                            out.insert(idx, "")
                        lastLineIsTag = True
            for line in out:
                snippet += (
                    "\n " + self.prefix + (self.indentSpaces + line if line else "")
                )
        else:
            snippet += (
                "\n "
                + self.prefix
                + self.indentSpaces
                + "${0:"
                + self.trailingString
                + "}"
            )

        snippet += "\n" + closer
        return snippet


class DocBlockrForkIndentCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        v = self.view
        currPos = v.sel()[0].begin()
        currLineRegion = v.line(currPos)
        currCol = currPos - currLineRegion.begin()  # which column we're currently in
        prevLine = v.substr(v.line(v.line(currPos).begin() - 1))
        spaces = self.getIndentSpaces(prevLine)
        if spaces:
            toStar = len(re.search("^(\\s*\\*)", prevLine).group(1))
            toInsert = spaces - currCol + toStar
            if spaces is None or toInsert <= 0:
                v.run_command("insert_snippet", {"contents": "\t"})
                return

            v.insert(edit, currPos, " " * toInsert)
        else:
            v.insert(edit, currPos, "\t")

    def getIndentSpaces(self, line):
        hasTypes = get_parser(self.view).settings["typeInfo"]
        extraIndent = "\\s+\\S+" if hasTypes else ""
        res = (
            re.search(
                "^\\s*\\*(?P<fromStar>\\s*@(?:param|property)%s\\s+\\S+\\s+)\\S"
                % extraIndent,
                line,
            )
            or re.search(
                "^\\s*\\*(?P<fromStar>\\s*@(?:returns?|define)%s\\s+\\S+\\s+)\\S"
                % extraIndent,
                line,
            )
            or re.search("^\\s*\\*(?P<fromStar>\\s*@[a-z]+\\s+)\\S", line)
            or re.search("^\\s*\\*(?P<fromStar>\\s*)", line)
        )
        if res:
            return len(res.group("fromStar"))
        return None


class DocBlockrForkJoinCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        v = self.view
        for sel in v.sel():
            for lineRegion in reversed(v.lines(sel)):
                v.replace(
                    edit,
                    v.find(
                        "[ \\t]*\\n[ \\t]*((?:\\*|//[!/]?|#)[ \\t]*)?",
                        lineRegion.begin(),
                    ),
                    " ",
                )


class DocBlockrForkDecorateCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        v = self.view
        re_whitespace = re.compile("^(\\s*)//")
        v.run_command("expand_selection", {"to": "scope"})
        for sel in v.sel():
            maxLength = 0
            lines = v.lines(sel)
            for lineRegion in lines:
                lineText = v.substr(lineRegion)
                tabCount = lineText.count("\t")
                leadingWS = len(re_whitespace.match(lineText).group(1))
                leadingWS = leadingWS - tabCount
                maxLength = max(maxLength, lineRegion.size())

            lineLength = maxLength - (leadingWS + tabCount)
            leadingWS = tabCount * "\t" + " " * leadingWS
            v.insert(edit, sel.end(), leadingWS + "/" * (lineLength + 3) + "\n")

            for lineRegion in reversed(lines):
                line = v.substr(lineRegion)
                rPadding = 1 + (maxLength - lineRegion.size())
                v.replace(edit, lineRegion, leadingWS + line + (" " * rPadding) + "//")
                # break

            v.insert(edit, sel.begin(), "/" * (lineLength + 3) + "\n")


class DocBlockrForkDeindentCommand(sublime_plugin.TextCommand):
    """
    When pressing enter at the end of a docblock, this takes the cursor back one space.
    /**
     *
     */|   <-- from here
    |      <-- to here
    """

    def run(self, edit):
        v = self.view
        lineRegion = v.line(v.sel()[0])
        line = v.substr(lineRegion)
        v.insert(edit, v.sel()[0].begin(), re.sub("^(\\s*)\\s\\*/.*", "\n\\1", line))


class DocBlockrForkReparseCommand(sublime_plugin.TextCommand):
    """
    Reparse a docblock to make the fields 'active' again, so that pressing tab will jump to the next one
    """

    def run(self, edit):
        tabIndex = counter()

        def tabStop(m):
            return "${%d:%s}" % (next(tabIndex), m.group(1))

        v = self.view
        v.run_command("clear_fields")
        sel = get_doc_block_region(v, v.sel()[0].begin())

        # escape string, so variables starting with $ won't be removed
        text = escape(v.substr(sel))

        # strip out leading spaces, since inserting a snippet keeps the indentation
        text = re.sub("\\n\\s+\\*", "\n *", text)

        # replace [bracketed] [text] with a tabstop
        text = re.sub("(\\[.+?\\])", tabStop, text)

        v.erase(edit, sel)
        write(v, text)


class DocBlockrForkTrimAutoWhitespaceCommand(sublime_plugin.TextCommand):
    """
    Trim the automatic whitespace added when creating a new line in a docblock.
    """

    def run(self, edit):
        v = self.view
        lineRegion = v.line(v.sel()[0])
        line = v.substr(lineRegion)
        spaces = max(0, v.settings().get("indentation_spaces", 1))
        v.replace(
            edit,
            lineRegion,
            re.sub("^(\\s*\\*)\\s*$", "\\1\n\\1" + (" " * spaces), line),
        )


class DocBlockrForkWrapLinesCommand(sublime_plugin.TextCommand):
    """
    Reformat description text inside a comment block to wrap at the correct length.
    Wrap column is set by the first ruler (set in Default.sublime-settings), or 80 by default.
    Shortcut Key: alt+q
    """

    def run(self, edit):
        v = self.view
        view_settings = v.settings()
        rulers = view_settings.get("rulers")
        tabSize = view_settings.get("tab_size")

        wrapLength = rulers[0] if (len(rulers) > 0) else 80
        numIndentSpaces = max(0, settings.get("doc_blockr_fork.indentation_spaces", 1))
        indentSpaces = " " * numIndentSpaces
        indentSpacesSamePara = " " * max(
            0,
            settings.get(
                "doc_blockr_fork.indentation_spaces_same_para", numIndentSpaces
            ),
        )
        spacerBetweenSections = settings.get("doc_blockr_fork.spacer_between_sections")
        spacerBetweenDescriptionAndTags = (
            settings.get("doc_blockr_fork.spacer_between_sections")
            == "after_description"
        )

        dbRegion = get_doc_block_region(v, v.sel()[0].begin())

        # find the first word
        startPoint = v.find(r"\n\s*\* ", dbRegion.begin()).begin()
        # find the first tag, or the end of the comment
        endPoint = v.find(r"\s*\n\s*\*(/)", dbRegion.begin()).begin()

        # replace the selection with this ^ new selection
        v.sel().clear()
        v.sel().add(sublime.Region(startPoint, endPoint))
        # get the description text
        text = v.substr(v.sel()[0])

        # find the indentation level
        indentation = len(
            re.sub("\t", " " * tabSize, re.search("\n(\\s*\\*)", text).group(1))
        )
        wrapLength -= indentation - tabSize

        # join all the lines, collapsing "empty" lines
        text = re.sub("\n(\\s*\\*\\s*\n)+", "\n\n", text)

        def wrapPara(para):
            para = re.sub("(\n|^)\\s*\\*\\s*", " ", para)

            # split the paragraph into words
            words = para.strip().split(" ")
            text = "\n"
            line = " *" + indentSpaces
            lineTagged = False  # indicates if the line contains a doc tag
            paraTagged = False  # indicates if this paragraph contains a doc tag
            lineIsNew = True
            tag = ""

            # join all words to create lines, no longer than wrapLength
            for i, word in enumerate(words):
                if not word and not lineTagged:
                    continue

                if lineIsNew and word[0] == "@":
                    lineTagged = True
                    paraTagged = True
                    tag = word

                if len(line) + len(word) >= wrapLength - 1:
                    # appending the word to the current line would exceed its
                    # length requirements
                    text += line.rstrip() + "\n"
                    line = " *" + indentSpacesSamePara + word + " "
                    lineTagged = False
                    lineIsNew = True
                else:
                    line += word + " "

                lineIsNew = False

            text += line.rstrip()
            return {
                "text": text,
                "lineTagged": lineTagged,
                "tagged": paraTagged,
                "tag": tag,
            }

        # split the text into paragraphs, where each paragraph is eighter
        # defined by an empty line or the start of a doc parameter
        paragraphs = re.split("\n{2,}|\n\\s*\\*\\s*(?=@)", text)
        wrappedParas = []
        text = ""
        for p, para in enumerate(paragraphs):
            # wrap the lines in the current paragraph
            wrappedParas.append(wrapPara(para))

        # combine all the paragraphs into a single piece of text
        for i in range(0, len(wrappedParas)):
            para = wrappedParas[i]
            last = i == len(wrappedParas) - 1

            nextIsTagged = not last and wrappedParas[i + 1]["tagged"]
            nextIsSameTag = nextIsTagged and para["tag"] == wrappedParas[i + 1]["tag"]

            if (
                last
                or (para["lineTagged"] or nextIsTagged)
                and not (spacerBetweenSections and not nextIsSameTag)
                and not (
                    not para["lineTagged"]
                    and nextIsTagged
                    and spacerBetweenDescriptionAndTags
                )
            ):
                text += para["text"]
            else:
                text += para["text"] + "\n *"

        text = escape(text)
        write(v, text)
