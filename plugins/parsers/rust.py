# -*- coding: utf-8 -*-
import re

from ._base import DocBlockrForkParser


class DocBlockrForkRust(DocBlockrForkParser):
    def setupParserOptions(self):
        self.options = {
            "curlyTypes": False,
            "typeInfo": False,
            "typeTag": False,
            "varIdentifier": ".*",
            "fnIdentifier": ".*",
            "fnOpener": "^\\s*fn",
            "commentCloser": " */",
            "bool": "Boolean",
            "function": "Function",
        }

    def parseFunction(self, line):
        res = re.search("\\s*fn\\s+(?P<name>\\S+)", line)
        if not res:
            return None

        name = res.group("name").join("")

        return (name, [])

    def formatFunction(self, name, args):
        return name
