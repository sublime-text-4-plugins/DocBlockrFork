# -*- coding: utf-8 -*-
import re

from ._base import DocBlockrForkParser


class DocBlockrForkCPP(DocBlockrForkParser):
    def setupParserOptions(self):
        nameToken = "[a-zA-Z_][a-zA-Z0-9_]*"
        identifier = "(%s)(::%s)?" % (nameToken, nameToken)
        self.options = {
            "typeInfo": False,
            "curlyTypes": False,
            "typeTag": "param",
            "commentCloser": " */",
            "fnIdentifier": identifier,
            "varIdentifier": "("
            + identifier
            + ")\\s*(?:\\[(?:"
            + identifier
            + r")?\]|\((?:(?:\s*,\s*)?[a-z]+)+\s*\))*",
            "fnOpener": identifier + "\\s+" + identifier + "\\s*\\(",
            "bool": "bool",
            "function": "function",
        }

    def parseFunction(self, line):
        res = re.search(
            "(?P<retval>"
            + self.options["varIdentifier"]
            + ")[&*\\s]+"
            + "(?P<name>"
            + self.options["varIdentifier"]
            + ");?"
            # void fnName
            # (arg1, arg2)
            + "\\s*\\(\\s*(?P<args>.*)\\)",
            line,
        )
        if not res:
            return None

        return (res.group("name"), res.group("args"), res.group("retval"))

    def parseArgs(self, args):
        if args.strip() == "void":
            return []
        return super(DocBlockrForkCPP, self).parseArgs(args)

    def getArgType(self, arg):
        return None

    def getArgName(self, arg):
        return re.search(self.options["varIdentifier"] + r"(?:\s*=.*)?$", arg).group(1)

    def parseVar(self, line):
        return None

    def guessTypeFromValue(self, val):
        return None

    def getFunctionReturnType(self, name, retval):
        return retval if retval != "void" else None
