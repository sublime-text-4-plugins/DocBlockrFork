# -*- coding: utf-8 -*-
import re

from ._base import DocBlockrForkParser
from ._base import is_numeric


class DocBlockrForkCoffeescript(DocBlockrForkParser):
    def setupParserOptions(self):
        identifier = "[a-zA-Z_$][a-zA-Z_$0-9]*"
        self.options = {
            # curly brackets around the type information
            "curlyTypes": True,
            "typeTag": self.options.get("override_js_var") or "type",
            "typeInfo": True,
            # technically, they can contain all sorts of unicode, but w/e
            "varIdentifier": identifier,
            "fnIdentifier": identifier,
            "fnOpener": None,  # no multi-line function definitions for you, hipsters!
            "commentCloser": "###",
            "bool": "Boolean",
            "function": "Function",
        }

    def parseFunction(self, line):
        res = re.search(
            #   fnName = function,  fnName : function
            "(?:(?P<name>"
            + self.options["varIdentifier"]
            + ")\\s*[:=]\\s*)?"
            + "(?:\\((?P<args>[^()]*?)\\))?\\s*([=-]>)",
            line,
        )
        if not res:
            return None

        # grab the name out of "name1 = function name2(foo)" preferring name1
        name = res.group("name") or ""
        args = res.group("args")

        return (name, args, None)

    def parseVar(self, line):
        res = re.search(
            #   var foo = blah,
            #       foo = blah;
            #   baz.foo = blah;
            #   baz = {
            #        foo : blah
            #   }
            "(?P<name>"
            + self.options["varIdentifier"]
            + ")\\s*[=:]\\s*(?P<val>.*?)(?:[;,]|$)",
            line,
        )
        if not res:
            return None

        return (res.group("name"), res.group("val").strip())

    def guessTypeFromValue(self, val):
        lowerPrimitives = self.settings.get(
            "doc_blockr_fork.lower_case_primitives", False
        )
        if is_numeric(val):
            return "number" if lowerPrimitives else "Number"
        if val[0] == '"' or val[0] == "'":
            return "string" if lowerPrimitives else "String"
        if val[0] == "[":
            return "Array"
        if val[0] == "{":
            return "Object"
        if val == "true" or val == "false":
            return "boolean" if lowerPrimitives else "Boolean"
        if re.match("RegExp\\b|\\/[^\\/]", val):
            return "RegExp"
        if val[:4] == "new ":
            res = re.search("new (" + self.options["fnIdentifier"] + ")", val)
            return res and res.group(1) or None
        return None
