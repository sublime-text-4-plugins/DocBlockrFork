# -*- coding: utf-8 -*-
import re

from ._base import DocBlockrForkParser
from ._base import is_numeric
from ._base import split_by_commas


class DocBlockrForkJavascript(DocBlockrForkParser):
    def setupParserOptions(self):
        identifier = "[a-zA-Z_$][a-zA-Z_$0-9]*"
        self.options = {
            # curly brackets around the type information
            "curlyTypes": True,
            "typeInfo": True,
            "typeTag": self.settings.get("doc_blockr_fork.override_js_var") or "type",
            # technically, they can contain all sorts of unicode, but w/e
            "varIdentifier": identifier,
            "fnIdentifier": identifier,
            "fnOpener": "(?:"
            + r"function[\s*]*(?:"
            + identifier
            + r")?\s*\("
            + "|"
            + "(?:"
            + identifier
            + r"|\(.*\)\s*=>)"
            + "|"
            + "(?:"
            + identifier
            + r"\s*\(.*\)\s*\{)"
            + ")",
            "commentCloser": " */",
            "bool": "Boolean",
            "function": "Function",
        }

    def parseFunction(self, line):
        res = (
            re.search(
                # Normal functions...
                #   fnName = function,  fnName : function
                r"(?:(?P<name1>"
                + self.options["varIdentifier"]
                + r")\s*[:=]\s*)?"
                + "function"
                # function fnName, function* fnName
                + r"(?P<generator>[\s*]+)?(?P<name2>"
                + self.options["fnIdentifier"]
                + ")?"
                # (arg1, arg2)
                + r"\s*\(\s*(?P<args>.*)\)",
                line,
            )
            or re.search(
                # ES6 arrow functions
                # () => y,  x => y,  (x, y) => y,  (x = 4) => y
                r"(?:(?P<args>"
                + self.options["varIdentifier"]
                + r")|\(\s*(?P<args2>.*)\))\s*=>",
                line,
            )
            or re.search(
                # ES6 method initializer shorthand
                # var person = { getName() { return this.name; } }
                r"(?P<name1>"
                + self.options["varIdentifier"]
                + ")\\s*\\((?P<args>.*)\\)\\s*\\{",
                line,
            )
        )
        if not res:
            return None

        groups = {"name1": "", "name2": "", "generator": "", "args": "", "args2": ""}
        groups.update(res.groupdict())
        # grab the name out of "name1 = function name2(foo)" preferring name1
        generatorSymbol = "*" if (groups["generator"] or "").find("*") > -1 else ""
        name = generatorSymbol + (groups["name1"] or groups["name2"] or "")
        args = groups["args"] or groups["args2"] or ""

        return (name, args, None)

    def parseVar(self, line):
        res = re.search(
            #   var foo = blah,
            #       foo = blah;
            #   baz.foo = blah;
            #   baz = {
            #        foo : blah
            #   }
            "(?P<name>"
            + self.options["varIdentifier"]
            + ")\\s*[=:]\\s*(?P<val>.*?)(?:[;,]|$)",
            line,
        )
        if not res:
            return None

        return (res.group("name"), res.group("val").strip())

    def getArgInfo(self, arg):
        if re.search("^\\{.*\\}$", arg):
            subItems = split_by_commas(arg[1:-1])
            prefix = "options."
        else:
            subItems = [arg]
            prefix = ""

        out = []
        for subItem in subItems:
            out.append((self.getArgType(subItem), prefix + self.getArgName(subItem)))

        return out

    def getArgType(self, arg):
        parts = re.split(r"\s*=\s*", arg, 1)
        # rest parameters
        if parts[0].find("...") == 0:
            return "...[type]"
        elif len(parts) > 1:
            return self.guessTypeFromValue(parts[1])

    def getArgName(self, arg):
        namePart = re.split(r"\s*=\s*", arg, 1)[0]

        # check for rest parameters, eg: function (foo, ...rest) {}
        if namePart.find("...") == 0:
            return namePart[3:]
        return namePart

    def getFunctionReturnType(self, name, retval):
        if name and name[0] == "*":
            return None
        return super(DocBlockrForkJavascript, self).getFunctionReturnType(name, retval)

    def getMatchingNotations(self, name):
        out = super(DocBlockrForkJavascript, self).getMatchingNotations(name)
        if name and name[0] == "*":
            # if '@returns' is preferred, then also use '@yields'. Otherwise, '@return' and '@yield'
            yieldTag = "@yield" + (
                "s"
                if self.settings.get("doc_blockr_fork.return_tag", "_")[-1] == "s"
                else ""
            )
            description = (
                " ${1:[description]}"
                if self.settings.get("doc_blockr_fork.return_description", True)
                else ""
            )
            out.append({"tags": ["%s {${1:[type]}}%s" % (yieldTag, description)]})
        return out

    def guessTypeFromValue(self, val):
        lowerPrimitives = self.settings.get(
            "doc_blockr_fork.lower_case_primitives", False
        )
        shortPrimitives = self.settings.get("doc_blockr_fork.short_primitives", False)
        if is_numeric(val):
            return "number" if lowerPrimitives else "Number"
        if val[0] == '"' or val[0] == "'":
            return "string" if lowerPrimitives else "String"
        if val[0] == "[":
            return "Array"
        if val[0] == "{":
            return "Object"
        if val == "true" or val == "false":
            returnVal = "Bool" if shortPrimitives else "Boolean"
            return returnVal.lower() if lowerPrimitives else returnVal
        if re.match("RegExp\\b|\\/[^\\/]", val):
            return "RegExp"
        if val.find("=>") > -1:
            return "function" if lowerPrimitives else "Function"
        if val[:4] == "new ":
            res = re.search("new (" + self.options["fnIdentifier"] + ")", val)
            return res and res.group(1) or None
        return None
