# -*- coding: utf-8 -*-
import re

from functools import reduce


def read_line(view, point):
    if point >= view.size():
        return

    next_line = view.line(point)
    return view.substr(next_line)


def split_by_commas(str):
    """
    Split a string by unenclosed commas: that is, commas which are not inside of quotes or brackets.
    split_by_commas('foo, bar(baz, quux), fwip = "hey, hi"')
     ==> ['foo', 'bar(baz, quux)', 'fwip = "hey, hi"']
    """
    out = []

    if not str:
        return out

    # the current token
    current = ""

    # characters which open a section inside which commas are not separators
    # between different arguments
    openQuotes = "\"'<({"
    # characters which close the section. The position of the character here should match the opening
    # indicator in `openQuotes`
    closeQuotes = "\"'>)}"

    matchingQuote = ""
    insideQuotes = False
    nextIsLiteral = False

    for char in str:
        if nextIsLiteral:  # previous char was a \
            current += char
            nextIsLiteral = False
        elif insideQuotes:
            if char == "\\":
                nextIsLiteral = True
            else:
                current += char
                if char == matchingQuote:
                    insideQuotes = False
        else:
            if char == ",":
                out.append(current.strip())
                current = ""
            else:
                current += char
                quoteIndex = openQuotes.find(char)
                if quoteIndex > -1:
                    matchingQuote = closeQuotes[quoteIndex]
                    insideQuotes = True

    out.append(current.strip())
    return out


def flatten(theList):
    """
    Flatten a shallow list. Only works when all items are lists.
    [[(1,1)], [(2,2), (3, 3)]] --> [(1,1), (2,2), (3,3)]
    """
    return [item for sublist in theList for item in sublist]


def escape(str):
    return str.replace("$", "\\$").replace("{", "\\{").replace("}", "\\}")


def is_numeric(val):
    try:
        float(val)
        return True
    except ValueError:
        return False


class DocBlockrForkParser(object):
    def __init__(self, settings):
        self.options = {}
        self.settings = settings
        self.setupParserOptions()
        self.nameOverride = None

    def isExistingComment(self, line):
        return re.search("^\\s*\\*", line)

    def setNameOverride(self, name):
        """overrides the description of the function - used instead of parsed description"""
        self.nameOverride = name

    def getNameOverride(self):
        return self.nameOverride

    def parse(self, line):
        if self.settings.get("doc_blockr_fork.simple_mode"):
            return None

        try:
            out = self.parseFunction(line)  # (name, args, retval, options)
            if out:
                return self.formatFunction(*out)

            out = self.parseVar(line)
            if out:
                return self.formatVar(*out)
        except BaseException:
            # TODO show exception if dev\debug mode
            return None

        return None

    def formatVar(self, name, val, valType=None):
        out = []
        if not valType:
            if not val or val == "":  # quick short circuit
                valType = "[type]"
            else:
                valType = (
                    self.guessTypeFromValue(val)
                    or self.guessTypeFromName(name)
                    or "[type]"
                )
        if self.inline:
            out.append(
                "@%s %s${1:%s}%s ${1:[description]}"
                % (
                    self.options["typeTag"],
                    "{" if self.options["curlyTypes"] else "",
                    valType,
                    "}" if self.options["curlyTypes"] else "",
                )
            )
        else:
            out.append("${1:[%s description]}" % (escape(name)))
            out.append(
                "@%s %s${1:%s}%s"
                % (
                    self.options["typeTag"],
                    "{" if self.options["curlyTypes"] else "",
                    valType,
                    "}" if self.options["curlyTypes"] else "",
                )
            )

        return out

    def getTypeInfo(self, argType, argName):
        typeInfo = ""
        if self.options["typeInfo"]:
            typeInfo = "%s${1:%s}%s " % (
                "{" if self.options["curlyTypes"] else "",
                escape(argType or self.guessTypeFromName(argName) or "[type]"),
                "}" if self.options["curlyTypes"] else "",
            )

        return typeInfo

    def formatFunction(self, name, args, retval, options={}):
        out = []
        if "as_setter" in options:
            out.append("@private")
            return out

        extraTagAfter = (
            self.settings.get("doc_blockr_fork.extra_tags_go_after") or False
        )

        description = self.getNameOverride() or (
            "[%s%sdescription]" % (escape(name), " " if name else "")
        )
        if self.settings.get("doc_blockr_fork.function_description"):
            out.append("${1:%s}" % description)

        if self.settings.get("doc_blockr_fork.autoadd_method_tag") is True:
            out.append("@%s %s" % ("method", escape(name)))

        if not extraTagAfter:
            self.addExtraTags(out)

        # if there are arguments, add a @param for each
        if args:
            # remove comments inside the argument list.
            args = re.sub(r"/\*.*?\*/", "", args)
            for argType, argName in self.parseArgs(args):
                typeInfo = self.getTypeInfo(argType, argName)

                format_str = "@param %s%s"
                if self.settings.get("doc_blockr_fork.param_description"):
                    format_str += " ${1:[description]}"

                out.append(
                    format_str
                    % (
                        typeInfo,
                        escape(argName)
                        if self.settings.get("doc_blockr_fork.param_name")
                        else "",
                    )
                )

        # return value type might be already available in some languages but
        # even then ask language specific parser if it wants it listed
        retType = self.getFunctionReturnType(name, retval)
        if retType is not None:
            typeInfo = ""
            if self.options["typeInfo"]:
                typeInfo = " %s${1:%s}%s" % (
                    "{" if self.options["curlyTypes"] else "",
                    retType or "[type]",
                    "}" if self.options["curlyTypes"] else "",
                )
            format_args = [
                self.settings.get("doc_blockr_fork.return_tag") or "@return",
                typeInfo,
            ]

            if self.settings.get("doc_blockr_fork.return_description"):
                format_str = "%s%s %s${1:[description]}"
                third_arg = ""

                # the extra space here is so that the description will align with the
                # param description
                if args and self.settings.get("doc_blockr_fork.align_tags") == "deep":
                    if not self.settings.get("doc_blockr_fork.per_section_indent"):
                        third_arg = " "

                format_args.append(third_arg)
            else:
                format_str = "%s%s"

            out.append(format_str % tuple(format_args))

        for notation in self.getMatchingNotations(name):
            if "tags" in notation:
                out.extend(notation["tags"])

        if extraTagAfter:
            self.addExtraTags(out)

        return out

    def getFunctionReturnType(self, name, retval):
        """returns None for no return type. False meaning unknown, or a string"""

        if re.match("[A-Z]", name):
            # no return, but should add a class
            return None

        if re.match("[$_]?(?:set|add)($|[A-Z_])", name):
            # setter/mutator, no return
            return None

        if re.match(
            "[$_]?(?:is|has)($|[A-Z_])", name
        ):  # functions starting with 'is' or 'has'
            return self.options["bool"]

        return self.guessTypeFromName(name) or False

    def parseArgs(self, args):
        """
        a list of tuples, the first being the best guess at the type, the second being the name
        """
        blocks = split_by_commas(args)
        out = []
        for arg in blocks:
            out.append(self.getArgInfo(arg))

        return flatten(out)

    def getArgInfo(self, arg):
        """
        Return a list of tuples, one for each argument derived from the arg param.
        """
        return [(self.getArgType(arg), self.getArgName(arg))]

    def getArgType(self, arg):
        return None

    def getArgName(self, arg):
        return arg

    def addExtraTags(self, out):
        extraTags = self.settings.get("doc_blockr_fork.extra_tags", [])
        if len(extraTags) > 0:
            out.extend(extraTags)

    def guessTypeFromName(self, name):
        matches = self.getMatchingNotations(name)
        if len(matches):
            rule = matches[0]
            if "type" in rule:
                return (
                    self.options[rule["type"]]
                    if rule["type"] in self.options
                    else rule["type"]
                )

        if re.match("(?:is|has)[A-Z_]", name):
            return self.options["bool"]

        if re.match("^(?:cb|callback|done|next|fn)$", name):
            return self.options["function"]

        return False

    def guessTypeFromValue(self, val):
        return None

    def getMatchingNotations(self, name):
        def checkMatch(rule):
            if "prefix" in rule:
                regex = re.escape(rule["prefix"])
                if re.match(".*[a-z]", rule["prefix"]):
                    regex += "(?:[A-Z_]|$)"
                return re.match(regex, name)
            elif "regex" in rule:
                return re.search(rule["regex"], name)

        return list(
            filter(checkMatch, self.settings.get("doc_blockr_fork.notation_map", []))
        )

    def getDefinition(self, view, pos):
        """
        get a relevant definition starting at the given point
        returns string
        """
        maxLines = 25  # don't go further than this
        openBrackets = 0

        definition = ""

        # count the number of open parentheses
        def countBrackets(total, bracket):
            return total + (1 if bracket == "(" else -1)

        for i in range(0, maxLines):
            line = read_line(view, pos)
            if line is None:
                break

            pos += len(line) + 1
            # strip comments
            line = re.sub(r"//.*", "", line)
            line = re.sub(r"/\*.*\*/", "", line)

            searchForBrackets = line

            # on the first line, only start looking from *after* the actual function starts. This is
            # needed for cases like this:
            # (function (foo, bar) { ... })
            if definition == "":
                opener = (
                    re.search(self.options["fnOpener"], line)
                    if self.options["fnOpener"]
                    else False
                )
                if opener:
                    # ignore everything before the function opener
                    searchForBrackets = line[opener.start():]

            openBrackets = reduce(
                countBrackets, re.findall("[()]", searchForBrackets), openBrackets
            )

            definition += line
            if openBrackets == 0:
                break
        return definition
