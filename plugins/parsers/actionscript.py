# -*- coding: utf-8 -*-
import re

from ._base import DocBlockrForkParser


class DocBlockrForkActionscript(DocBlockrForkParser):
    def setupParserOptions(self):
        nameToken = "[a-zA-Z_][a-zA-Z0-9_]*"
        self.options = {
            "typeInfo": False,
            "curlyTypes": False,
            "typeTag": "",
            "commentCloser": " */",
            "fnIdentifier": nameToken,
            "varIdentifier": "(%s)(?::%s)?" % (nameToken, nameToken),
            "fnOpener": "function(?:\\s+[gs]et)?(?:\\s+" + nameToken + ")?\\s*\\(",
            "bool": "bool",
            "function": "function",
        }

    def parseFunction(self, line):
        res = re.search(
            #   fnName = function,  fnName : function
            "(?:(?P<name1>"
            + self.options["varIdentifier"]
            + ")\\s*[:=]\\s*)?"
            + "function(?:\\s+(?P<getset>[gs]et))?"
            # function fnName
            + "(?:\\s+(?P<name2>" + self.options["fnIdentifier"] + "))?"
            # (arg1, arg2)
            + "\\s*\\(\\s*(?P<args>.*)\\)",
            line,
        )
        if not res:
            return None

        name = (
            res.group("name1")
            and re.sub(self.options["varIdentifier"], r"\1", res.group("name1"))
            or res.group("name2")
            or ""
        )

        args = res.group("args")
        options = {}
        if res.group("getset") == "set":
            options["as_setter"] = True

        return (name, args, None, options)

    def parseVar(self, line):
        return None

    def getArgName(self, arg):
        return re.sub(self.options["varIdentifier"] + r"(\s*=.*)?", r"\1", arg)

    def getArgType(self, arg):
        # could actually figure it out easily, but it's not important for the documentation
        return None
