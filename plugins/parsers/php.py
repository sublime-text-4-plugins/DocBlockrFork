# -*- coding: utf-8 -*-
import re

from ._base import DocBlockrForkParser
from ._base import is_numeric


class DocBlockrForkPHP(DocBlockrForkParser):
    def setupParserOptions(self):
        shortPrimitives = self.settings.get("doc_blockr_fork.short_primitives", False)
        nameToken = "[a-zA-Z_\\x7f-\\xff][a-zA-Z0-9_\\x7f-\\xff]*"
        self.options = {
            # curly brackets around the type information
            "curlyTypes": False,
            "typeInfo": True,
            "typeTag": "var",
            "varIdentifier": "&?[$]" + nameToken + "(?:->" + nameToken + ")*",
            "fnIdentifier": nameToken,
            "typeIdentifier": "\\\\?" + nameToken + "(\\\\" + nameToken + ")*",
            "fnOpener": "function(?:\\s+" + nameToken + ")?\\s*\\(",
            "commentCloser": " */",
            "bool": "bool" if shortPrimitives else "boolean",
            "function": "function",
        }

    def parseFunction(self, line):
        res = re.search(
            "function\\s+&?\\s*" + "(?P<name>" + self.options["fnIdentifier"] + ")"
            # function fnName
            # (arg1, arg2)
            + "\\s*\\(\\s*(?P<args>.*)\\)",
            line,
        )
        if not res:
            return None

        return (res.group("name"), res.group("args"), None)

    def getArgType(self, arg):

        res = re.search(
            "(?P<type>"
            + self.options["typeIdentifier"]
            + ")?"
            + "\\s*(?P<name>"
            + self.options["varIdentifier"]
            + ")"
            + "(\\s*=\\s*(?P<val>.*))?",
            arg,
        )

        if res:

            argType = res.group("type")
            # argName = res.group("name")
            argVal = res.group("val")

            # function fnc_name(type $name = val)
            if argType and argVal:

                # function fnc_name(array $x = array())
                # function fnc_name(array $x = [])
                argValType = self.guessTypeFromValue(argVal)
                if argType == argValType:
                    return argType

                # function fnc_name(type $name = null)
                return argType + "|" + argValType

            # function fnc_name(type $name)
            if argType:
                return argType

            # function fnc_name($name = value)
            if argVal:
                guessedType = self.guessTypeFromValue(argVal)
                return guessedType if guessedType != "null" else None
        # function fnc_name()
        return None

    def getArgName(self, arg):
        return re.search(
            "(" + self.options["varIdentifier"] + ")(?:\\s*=.*)?$", arg
        ).group(1)

    def parseVar(self, line):
        res = re.search(
            #   var $foo = blah,
            #       $foo = blah;
            #   $baz->foo = blah;
            #   $baz = array(
            #        'foo' => blah
            #   )
            "(?P<name>"
            + self.options["varIdentifier"]
            + ")\\s*=>?\\s*(?P<val>.*?)(?:[;,]|$)",
            line,
        )
        if res:
            return (res.group("name"), res.group("val").strip())

        res = re.search(
            "\\b(?:var|public|private|protected|static)\\s+(?P<name>"
            + self.options["varIdentifier"]
            + ")",
            line,
        )
        if res:
            return (res.group("name"), None)

        return None

    def guessTypeFromValue(self, val):
        shortPrimitives = self.settings.get("doc_blockr_fork.short_primitives", False)
        if is_numeric(val):
            return "float" if "." in val else "int" if shortPrimitives else "integer"
        if val[0] == '"' or val[0] == "'":
            return "string"
        if val[:5] == "array" or (val[0] == "[" and val[-1] == "]"):
            return "array"
        if val.lower() in ("true", "false", "filenotfound"):
            return "bool" if shortPrimitives else "boolean"
        if val[:4] == "new ":
            res = re.search("new (" + self.options["fnIdentifier"] + ")", val)
            return res and res.group(1) or None
        if val.lower() in ("null"):
            return "null"
        return None

    def getFunctionReturnType(self, name, retval):
        shortPrimitives = self.settings.get("doc_blockr_fork.short_primitives", False)
        if name[:2] == "__":
            if name in ("__construct", "__destruct", "__set", "__unset", "__wakeup"):
                return None
            if name == "__sleep":
                return "array"
            if name == "__toString":
                return "string"
            if name == "__isset":
                return "bool" if shortPrimitives else "boolean"
        return DocBlockrForkParser.getFunctionReturnType(self, name, retval)
